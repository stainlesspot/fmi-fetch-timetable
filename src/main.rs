//#[macro_use]
mod page;
mod transfer;
mod utils;

use page::{Page, PageNotTimetablesError, TimetablesPage};
use std::{convert::TryFrom, env, path::Path};

fn get_num_arg(n: usize) -> Option<u32> {
    env::args().nth(n).and_then(|s| s.parse().ok())
}

fn main() -> Result<(), ureq::Error> {
    let start = get_num_arg(1)
        .expect("First argument should be the start of the id range");
    let end = get_num_arg(2)
        .expect("Second argument should be the end of the id range");

    let dir = Path::new("downloads");
    for i in start..=end {
        match Page::from_id(i).map(|mp| mp.map(TimetablesPage::try_from)) {
            Err(err) => {
                println!("fetching page {} failed: {}", i, err);
            }
            Ok(None) => {
                println!("page {} not found", i);
            }
            Ok(Some(Err(PageNotTimetablesError))) => {
                println!("page {} not a timetables page", i);
            }
            Ok(Some(Ok(page))) => {
                println!("downloading page {}", i);
                page.download_to(dir.join(i.to_string())).unwrap_or_else(
                    |err| println!("    download failed: {}", err),
                )
            }
        }
    }

    //if let Some(title) = page_title(&html) {
    //    println!("hit {}", title);
    //}

    Ok(())
}
