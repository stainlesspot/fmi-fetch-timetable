use std::io::{self, Read, Write};

pub struct BufTransfer<R: Read, W: Write> {
    buf: Vec<u8>,
    reader: R,
    writer: W,
}

impl<R: Read, W: Write> BufTransfer<R, W> {
    pub fn new(capacity: usize, reader: R, writer: W) -> Self {
        let buf = vec![0; capacity];
        Self {
            buf,
            reader,
            writer,
        }
    }
}

impl<R: Read, W: Write> Iterator for BufTransfer<R, W> {
    type Item = io::Result<usize>;

    fn next(&mut self) -> Option<Self::Item> {
        self.reader
            .read(&mut self.buf)
            .map(|len| if len == 0 { None } else { Some(len) })
            .transpose()
            .map(|res| {
                res.and_then(|len| {
                    self.writer.write_all(&self.buf[..len]).map(|_| len)
                })
            })
    }
}
