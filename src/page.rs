use crate::transfer::BufTransfer;
use crate::utils::selector;
use scraper::element_ref::ElementRef;
use scraper::Html;
use std::{convert::TryFrom, fs, io, path::Path};

const BUF_SIZE: usize = 2048;

pub fn fmi_node(id: u32) -> String {
    "https://www.fmi.uni-sofia.bg/bg/node/".to_string() + &id.to_string()
}

pub struct Page {
    html: Html,
    title: String,
}

impl Page {
    pub fn from_id(id: u32) -> Result<Option<Self>, ureq::Error> {
        let html = Html::parse_document(
            &ureq::get(&fmi_node(id)).call()?.into_string()?,
        );
        let maybe_title = html
            .select(&selector("#page-title"))
            .next()
            .map(|h| h.text().collect::<String>());

        Ok(maybe_title.map(|title| Self { html, title }))
    }
}

struct TimetableLink {
    name: String,
    url: String,
}

impl TimetableLink {
    fn from_anchor_ref(a: ElementRef) -> Option<TimetableLink> {
        let target: String = a.value().attr("href")?.into();
        if !target.contains("intranet.fmi.uni-sofia.bg") {
            return None;
        }

        let url = target + "/download";
        let name = a
            .text()
            .collect::<String>()
            .trim()
            .replace(&[' ', ','][..], "-")
            .replace('"', "")
            + ".pdf";

        Some(TimetableLink { name, url })
    }
}

pub struct PageNotTimetablesError;

pub struct TimetablesPage {
    timetable_links: Vec<TimetableLink>,
}

impl TryFrom<Page> for TimetablesPage {
    type Error = PageNotTimetablesError;

    fn try_from(page: Page) -> Result<Self, Self::Error> {
        if page
            .html
            .select(&selector(
                "#breadcrumb a[href='https://www.fmi.uni-sofia.bg/bg/razpis']",
            ))
            .next()
            .is_some()
            && page.title.to_lowercase().contains("разпис")
        {
            let timetable_links = page
                .html
                .select(&selector("#main a"))
                .filter_map(TimetableLink::from_anchor_ref)
                .collect();

            Ok(Self { timetable_links })
        } else {
            Err(PageNotTimetablesError)
        }
    }
}

impl TimetablesPage {
    pub fn download_to<P: Clone + AsRef<Path>>(
        &self,
        dir: P,
    ) -> Result<(), ureq::Error> {
        fs::create_dir_all(dir.clone())?;

        for TimetableLink { name, url } in self.timetable_links.iter() {
            let reader = ureq::get(&url).call()?.into_reader();
            let file_writer = fs::File::create(dir.as_ref().join(name))?;

            BufTransfer::new(BUF_SIZE, reader, file_writer)
                .collect::<Result<Vec<usize>, io::Error>>()?;
        }
        Ok(())
    }
}
