use scraper::Selector;
//
//macro_rules! selector {
//    ($expr:expr) => {
//        &scraper::Selector::parse($expr).unwrap()
//    };
//}

pub fn selector(sel: &str) -> Selector {
    Selector::parse(sel).unwrap()
}
